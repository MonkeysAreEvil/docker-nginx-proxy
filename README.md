# Description

A Docker compose template for proxying all my websites, using the default, public nginx image.

# Install

1. Edit all the template files, replace the env var blocks with appropriate values.

2. Run `docker-compose up`

# Licence

GPL v3+
